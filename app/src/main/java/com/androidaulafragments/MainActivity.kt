package com.androidaulafragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton

class MainActivity : AppCompatActivity(), FragmentListener {

    var btnNext : AppCompatButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnNext?.findViewById<AppCompatButton>(R.id.btn_next)
        btnNext?.setOnClickListener{goToNextFragment()}
        goToNextFragment()
    }

    override fun goToNextFragment() {
        val numberToDisplay = supportFragmentManager.backStackEntryCount + 1
        val fragContent = ContentFragment(numberToDisplay)
        val fragCrumb = BreadcrumbsFragment(numberToDisplay)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout,fragContent)
        fragmentTransaction.add(R.id.frameBreadcrumbsLayout,fragCrumb)
        fragmentTransaction.addToBackStack(numberToDisplay.toString())

        fragmentTransaction.commit()
    }

    override fun goToBackFragment(number: Int){
        val fragToGoBackTo = number.toString()
        when{
            fragToGoBackTo.isBlank() -> supportFragmentManager.popBackStack()
            else -> supportFragmentManager.popBackStack(fragToGoBackTo,0)
        }
    }
}
