package com.androidaulafragments

interface FragmentListener {
    fun goToNextFragment()
    fun goToBackFragment(goToFragmentNumber: Int)
}