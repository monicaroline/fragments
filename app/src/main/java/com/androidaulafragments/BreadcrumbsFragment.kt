package com.androidaulafragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment

class BreadcrumbsFragment(private val numberToDisplay: Int) : Fragment() {

    private var breadNumber: AppCompatTextView? = null
    private var nextFragmentListener: FragmentListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        nextFragmentListener = context as? FragmentListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_breadcrumb,container,false)
        view?.let {
           breadNumber = view.findViewById(R.id.crumb_text)
            breadNumber?.text = numberToDisplay.toString()
            breadNumber?.setOnClickListener { nextFragmentListener?.goToBackFragment(numberToDisplay) }
        }
        return view
    }
}