package com.androidaulafragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView

class ContentFragment(private val number: Int) : Fragment() {

    private var tvNumber: AppCompatTextView? = null
    private var btnGoToNext: AppCompatButton? = null
    private var nextFragmentListener: FragmentListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        nextFragmentListener = context as? FragmentListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_content, container, false)
        view?.let {
            tvNumber = view.findViewById(R.id.tv_current_frag)
            btnGoToNext = view.findViewById(R.id.btn_next)
            tvNumber?.text = "Fragment: $number"
            btnGoToNext?.setOnClickListener { nextFragmentListener?.goToNextFragment() }
        }
        return view
    }
}
